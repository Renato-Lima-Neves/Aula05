const express = require('express');
const app = express();

app.get('/', (req, res) => {
    console.log('CHAMADO VIA GET!');
    res.send('<h1>METHOD GET - Route: /</h1>');
});

app.get('/usuario', (req, res) => {
    console.log('CHAMADO VIA GET! - consulta usuário');
    res.send('<h1>METHOD GET - Route: /usuario</h1>');
});

app.listen(3000, function () {
    console.log('projeto iniciado na porta 3000');
});