const{createServer} = require('http');

let server = createServer(
    (req, res) => {
        res.writeHead(200, {'content-type': 'text/html'});
        res.write(`<h1>ABCDE</h1>`);
        res.end();
    }
);

server.listen(8000);

console.log('Projeto iniciado na porta 8000');